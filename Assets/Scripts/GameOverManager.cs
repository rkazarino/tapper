﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    private const string YOUR_SCORE = "Your Score: ";

    [SerializeField]
    private Text score;

    private void Start()
    {
        score.text = YOUR_SCORE + GameManager.instance.getScore().ToString();
        if (System.Convert.ToInt64(PlayerPrefs.GetString(BestScoreManager.SCORE)) < GameManager.instance.getScore())
            PlayerPrefs.SetString(BestScoreManager.SCORE, GameManager.instance.getScore().ToString());

    }


}
