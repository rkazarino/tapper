﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private float time = 0f;
    private bool isTimerStart = false;

    private long score = 0L;
    public static GameManager instance { get; set; }


    private GameManager() { }

    void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }



    public void startTimer()
    {
        if (isTimerStart)
            stopTimer();

        isTimerStart = true;
    }

    public void stopTimer()
    {
        isTimerStart = false;
        time = 0;
    }

    void Update()
    {
        if (isTimerStart)
        {
            time += Time.deltaTime;
        }
    }

    public void OpenMainScreen()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
    }

    public void OpenStartScreen()
    {
        this.score = 0;
        SceneManager.LoadScene("StartScene", LoadSceneMode.Single);
    }
    public void OpenGameOverScreen(long score)
    {
        this.score = score;
        SceneManager.LoadScene("GameOverScene", LoadSceneMode.Single);
    }

    public long getScore()
    {
        return score;
    }

    public float getTime()
    {
        return time;
    }

    public float getSpeedAcselerator()
    {
        float denominator = 30;
        return time / denominator;
    }

}
