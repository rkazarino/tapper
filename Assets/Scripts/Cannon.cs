﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cannon : MovableObject
{
    private const float MIN_FIRE_DELAY_TIME = 1f;
    private const float MAX_FIRE_DELAY_TIME = 3f;
    private const float MAX_ANIMATOR_SPEED = 10;
    private const float MAX_SPEED = 20;

    private Animator animator;
    public RectTransform rails { get; set; }

    public float startFireFrequency { get; set; } = 1f;
    public float currentFireFrequency { get; set; }
    public float startSpeed { get; set; } = 7f;
    public float startAnimatorSpeed { get; set; } = 1f;

    private float startPosition;
    private float endPosition;
    private bool isFire = false;

    protected override void Start()
    {
        base.Start();
        Initialize();
        StartCoroutine(Fire());
    }

    protected override void Update()
    {
        UpdateSpeed();

        if (isFire)
        {
            currentSpeed = 0f;
        }

        base.Update();

        Debug.Log("cannon speed: " + currentSpeed);
        Debug.Log("cannon animator speed: " + animator.speed);
        Debug.Log("cannon fire frequency: " + currentFireFrequency);
    }

    protected virtual void UpdateSpeed()
    {
        float speedAcselerator = GameManager.instance.getSpeedAcselerator();
        float newSpeed = startSpeed + speedAcselerator;
        float newAnimatorSpeed = startAnimatorSpeed + speedAcselerator;

        if (newAnimatorSpeed < MAX_ANIMATOR_SPEED)
            animator.speed = newAnimatorSpeed;

        if (newSpeed < MAX_SPEED)
            currentSpeed = newSpeed;

        currentFireFrequency = startFireFrequency + speedAcselerator;
    }

    IEnumerator Fire()
    {
        yield return new WaitForSeconds(Random.Range(MIN_FIRE_DELAY_TIME / currentFireFrequency, MAX_FIRE_DELAY_TIME / currentFireFrequency));
        isFire = true;
        animator.SetBool("isFire", isFire);
    }

    public void FireFinishEvent()
    {
        CreateProjectile();
        isFire = false;
        animator.SetBool("isFire", isFire);
        StartCoroutine(Fire());
    }

    public void CreateProjectile()
    {
        float projectileXBias = 1.5f;
        float projectileYBias = 0.5f;
        Instantiate(ProjectileGenerator.instance.getProjectile(), new Vector3(transform.position.x + projectileXBias, transform.position.y + projectileYBias, transform.position.z), Quaternion.identity, rails.transform);
    }

    private void Initialize()
    {
        GameObject canvasObject = GameObject.Find("MainCanvas");
        CanvasScaler canvasScaler = canvasObject.GetComponent<CanvasScaler>();
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        float cannonHeight = sprite.bounds.size.y * sprite.pixelsPerUnit * canvasScaler.matchWidthOrHeight;
        float amplitude = rails.rect.height - cannonHeight;
        animator = GetComponent<Animator>();

        currentFireFrequency = startFireFrequency;
        currentSpeed = startSpeed;
        startPosition = -amplitude / 2;
        endPosition = +amplitude / 2;
        transform.localPosition = new Vector3(0, Random.Range(-amplitude / 2, amplitude / 2), 0);
        ChangeDirection(Vector3.up);
    }


    protected override void Moving()
    {
        if (transform.localPosition.y >= endPosition)
            ChangeDirection(Vector3.down);
        if (transform.localPosition.y <= startPosition)
            ChangeDirection(Vector3.up);

        base.Moving();
    }
}
