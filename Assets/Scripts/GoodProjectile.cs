﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodProjectile : Projectile
{
    [SerializeField]
    private GameObject track;


    protected override void DropDown()
    {
        player.Damage();
    }

    protected override void Collect()
    {
        var destroiTime = 1;

        player.AddScore(1);

        var trackInstance = Instantiate(track, transform.position, Quaternion.identity, transform.parent);
        trackInstance.GetComponent<SpriteRenderer>().sortingOrder = -10;
        Destroy(trackInstance, destroiTime);
    }









}
