﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileGenerator : MonoBehaviour
{
    public static ProjectileGenerator instance { get; set; }

    [SerializeField]
    List<GameObject> goodProjectiles = new List<GameObject>();
    [SerializeField]
    List<GameObject> badProjectiles = new List<GameObject>();
    [SerializeField]
    List<GameObject> bonusProjectiles = new List<GameObject>();

    private float goodProjectileProbability = 10;
    private float badProjectileProbability = 5;
    private float bonusProjectileProbability = 1;

    private ProjectileGenerator()
    {

    }

    private void Start()
    {
        instance = this;
    }

    public GameObject getProjectile()
    {
        float sum = goodProjectileProbability + badProjectileProbability + bonusProjectileProbability;
        float probability = Random.Range(0, sum);

        if (probability < bonusProjectileProbability)
        {
            return getRandomProjectileFromList(bonusProjectiles);
        }
        else if (probability < badProjectileProbability + bonusProjectileProbability)
        {
            return getRandomProjectileFromList(badProjectiles);
        }
        else
        {
            return getRandomProjectileFromList(goodProjectiles);
        }
    }

    private GameObject getRandomProjectileFromList(List<GameObject> projectiles)
    {
        if (projectiles.Count > 0)
            return projectiles[Random.Range(0, projectiles.Count)];
        else
            return null;
    }
}
