﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MovableObject
{
    private float MAX_SPEED = 20;

    public float startSpeed { get; set; } = 7f;

    private SpriteRenderer spriteRenderer;
    private Camera mainCamera;

    private float spritedWidth;

    protected Player player = Player.instance;

    protected abstract void DropDown();
    protected abstract void Collect();


    protected override void Start()
    {
        base.Start();
        Initialize();
    }

    private void Initialize()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        mainCamera = Camera.main;
        spritedWidth = (spriteRenderer.sprite.pixelsPerUnit * spriteRenderer.bounds.size.x);
        ChangeDirection(Vector3.right);
        updateSpeed();
    }

    protected override void Update()
    {

        if (mainCamera.WorldToScreenPoint(transform.position).x > mainCamera.pixelWidth + spritedWidth / 2)
        {
            Destroy(gameObject);
            DropDown();
            return;
        }

        base.Update();
    }


    protected virtual void updateSpeed()
    {
        float speedAcselerator = GameManager.instance.getSpeedAcselerator();
        float newSpeed = speedAcselerator + startSpeed;

        if (newSpeed < MAX_SPEED)
        {
            currentSpeed = newSpeed;
        }

    }

    protected virtual void OnMouseDown()
    {

        Collect();
        Destroy(gameObject);
    }
}
