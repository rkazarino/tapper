﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BadProjectile : Projectile
{

    private GameObject damageBlure;

    private Animator animator;

    protected override void Start()
    {
        base.Start();
        Initialize();
    }

    private void Initialize()
    {
        damageBlure = GameObject.Find("DamageBlur");
        animator = GetComponent<Animator>();
    }
    protected override void Collect()
    {
        damageBlure.GetComponent<Image>().enabled = true;
        currentSpeed = 0;
        animator.SetTrigger("explose");
        player.Damage();
    }

    private void ExplosionFinish()
    {
        Destroy(gameObject);
        damageBlure.GetComponent<Image>().enabled = false;
    }

    protected virtual void OnMouseDown()
    {
        Collect();
    }

    protected override void DropDown()
    {

    }
}
