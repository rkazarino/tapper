﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reils : MonoBehaviour
{
    [SerializeField]
    private GameObject cannonPrefab;

    private bool[] isCannonGenerate = { false, false, false, false, false };

    void Update()
    {
        for (int i = 0; i < isCannonGenerate.Length; i++)
        {
            if (!isCannonGenerate[i])
            {
                if (GameManager.instance.getTime() >= i * 60)
                {
                    generateCannon();
                    isCannonGenerate[i] = true;
                }
                break;
            }
        }
    }

    private void generateCannon()
    {
        GameObject newCannon = Instantiate(cannonPrefab, new Vector3(0, 0, 0), Quaternion.identity, transform);
        newCannon.GetComponent<Cannon>().rails = (RectTransform)transform;
    }

}
