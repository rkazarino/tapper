﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player instance { get; set; }

    private Player() { }

    [SerializeField]
    private int startHealtPont = 5;
    [SerializeField]
    private Text healthText;
    [SerializeField]
    private Text scoreText;

    private int currentHealtPont;
    private long currentScore = 0L;

    public void Damage()
    {
        currentHealtPont--;
        if (currentHealtPont <= 0)
        {
            GameManager.instance.OpenGameOverScreen(currentScore);
        }
    }

    public void AddScore(int score)
    {
        currentScore += score;
    }

    public void addHealthPoint()
    {
        currentHealtPont++;
    }

    void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        instance = this;

        GameManager.instance.startTimer();
        currentScore = 0L;
        currentHealtPont = startHealtPont;
        updateUI();
    }

    void Update()
    {
        updateUI();
    }

    private void updateUI()
    {
        if (healthText != null)
            healthText.text = currentHealtPont.ToString();

        if (scoreText != null)
            scoreText.text = currentScore.ToString();
    }

    public long getScore()
    {
        return currentScore;
    }

}
