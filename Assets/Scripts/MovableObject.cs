﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObject : MonoBehaviour
{
    protected float currentSpeed { get; set; } = 4;
    private Vector3 direction = Vector3.zero;

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        Moving();
    }

    protected virtual void Moving()
    {
        transform.Translate(direction * currentSpeed * Time.deltaTime);
    }

    protected virtual void ChangeDirection(Vector3 direction)
    {
        this.direction = direction;
    }






}
