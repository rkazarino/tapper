﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthProjectile : Projectile
{
    protected override void Start()
    {
        base.Start();
        startSpeed += 1;
    }

    protected override void Collect()
    {
        player.addHealthPoint();
    }

    protected override void DropDown() { }
}
