﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BestScoreManager : MonoBehaviour
{
    public const string SCORE = "score";
    private const string BEST_SCORE = "Best Score: ";

    // Start is called before the first frame update
    void Start()
    {
        var score = PlayerPrefs.GetString(SCORE);

        if (score == "" || score == null)
        {
            score = "0";
            PlayerPrefs.SetString(SCORE, score);
        }

        GetComponent<Text>().text = BEST_SCORE + score;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
